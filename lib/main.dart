import 'package:flutter/material.dart';
import './screem/home.dart';

/*void main() {
  runApp(new HelloFlutterApp());
}*/

void main() => runApp(new HelloFlutterApp());

class HelloFlutterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Title of the app",
      home: Scaffold(
          appBar: AppBar(
            title: Text("Tittle in the App bar"),
          ),
          body: Home()),
    );
  }
}